-- sub files
local MP = minetest.get_modpath(minetest.get_current_modname()) .. "/ui"
dofile(MP .. "/board.lua")
dofile(MP .. "/claim.lua")
dofile(MP .. "/details.lua")
dofile(MP .. "/new.lua")
