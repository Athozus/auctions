local S = minetest.get_translator("auctions")

function auctions.show_details(name, id, collision)
    local inv_name = "details:" .. id
    if not minetest.get_inventory({ type="detached", name=inv_name }) then
        minetest.create_detached_inventory(inv_name)
        local inv = minetest.get_inventory({ type="detached", name=inv_name })
        inv:set_size("main", 36)
        inv:set_width("main", 9)
    end
    local inv = minetest.get_inventory({ type="detached", name=inv_name })

    local auction = auctions.get_auction(id)

    -- fill the temporary inventory with auction items
    if inv:is_empty("main") then
        for _, item in ipairs(auction.items) do
            inv:add_item("main", ItemStack(item))
        end
    end

    if not collision then collision = "none" end
    local collision_strings = {
        owner = S("You're the owner of the auction"),
        nan = S("Not a number!"),
        money = S("You don't have enough money in your account"),
        increment = S("You didn't have incremented enough"),
        claim = S("Your claim inventory is not empty"),
        none = ""
    }
    local collision_str = collision_strings[collision]

    local details_formspec = [[
        formspec_version[4]
        size[17,9;]

        label[5.5,5.5;]] .. S("Auction inventory") .. [[]

        label[0.5,1;]] .. S("ID") .. [[: #%s]
        label[0.5,1.5;]] .. S("Title") .. [[: %s]
        label[0.5,2.0;]] .. S("Owner") .. [[: %s]
        textarea[0.5,2.5;4,1.5;;]] .. S("Description") .. [[:;%s]
        label[0.5,4.5;]] .. S("Initial bid") .. [[: %s]
        label[0.5,5;]] .. S("Increment") .. [[: %s]
        label[0.5,5.5;]] .. S("Fee") .. [[: %s]
        label[0.5,6;]] .. S("Timeout") .. [[: %s]
        textarea[10,6;2,2.5;;;%s]
        ]]

        if os.time() <= auction.timeout then
            details_formspec = details_formspec .. [[
            field[5.5,6;4,0.8;bidvalue;]] .. S("Bid") .. [[;%s]
            button[5.5,7.0;4.0,0.8;bid;]] .. S("New bid") .. [[]
            ]]
        elseif auction.bids[#auction.bids][2] == name and not auction.claimed then
            details_formspec = details_formspec .. [[
            button[5.5,7.0;4.0,0.8;claim;]] .. S("Claim") .. [[]
            ]]
        end

        details_formspec = details_formspec .. [[
        button[0.5,7.7;1.5,0.8;back;]] .. S("Back") .. [[]

        list[detached:]] .. inv_name .. [[;main;5.5,0.5;9,4]

        tablecolumns[color;text;text]
        table[12.5,6;4,2.5;bids;#999,]] .. S("Bid") .. "," .. S("Player")
    local formspec = { details_formspec }

    for i, bid in ipairs(auction.bids) do
        if string.sub(bid[2], 1, 1) == ":" then -- show minimum bid in blue
            formspec[#formspec+1] = "," .. "#6cf"
        elseif i == #auction.bids and os.time() > auction.timeout then -- show winner in yellow
            formspec[#formspec+1] = "," .. "#ffd700"
        else
            formspec[#formspec+1] = "," .. "#fff"
        end
        formspec[#formspec+1] = "," .. bid[1]
        if bid[2] == ":min" then
            formspec[#formspec+1] = "," .. S("Initial bid")
        else
            formspec[#formspec+1] = "," .. bid[2]
        end
    end

    formspec[#formspec+1] = "]"

    formspec = string.format(table.concat(formspec, ""),
    id,
    auction.title,
    auction.owner,
    auction.description,
    auction.bids[1][1],
    auction.increment,
    auction.fee,
    os.date("%Y-%m-%d %X", auction.timeout),
    collision_str,
    auction.bids[#auction.bids][1] + (#auction.bids == 1 and 0 or auction.increment)
    )

    minetest.show_formspec(name, "auctions:details", formspec)
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "auctions:details" then
        return
    end

    local name = player:get_player_name()
    local id = auctions.selected_idxs.auction[name]
    local auction = auctions.get_auction(id)

    if fields.back then
        auctions.show_board(name)
        return

    elseif fields.bid then
        local bid_value = tonumber(fields.bidvalue)
        if auction.owner == name then
            auctions.show_details(name, id, "owner")
        elseif type(bid_value) ~= "number" then
            auctions.show_details(name, id, "nan")
        elseif atm.balance[name] < bid_value + auction.fee then
            auctions.show_details(name, id, "money")
        elseif bid_value < auction.bids[#auction.bids][1] + (#auction.bids == 1 and 0 or auction.increment) then
            auctions.show_details(name, id, "increment")
        else
            auctions.new_bid(id, name, bid_value)
            auctions.show_details(name, id)
        end

    elseif fields.claim then
        if #auctions.get_claim(player) > 0 then -- not empty
            auctions.show_details(name, id, "claim")
        else
            auctions.claim_auction(id, name)
            auctions.show_claim(name)
        end
    end

    return true
end)
