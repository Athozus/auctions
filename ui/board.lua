local S = minetest.get_translator("auctions")

function auctions.show_board(name)
    local board_formspec = [[
        formspec_version[4]
        size[16,10;]

        label[0.5,0.75;]] .. S("Board") .. [[]

        button[10,0.5;2.5,0.8;new;]] .. S("New auction") .. [[]
        button[13,0.5;2.5,0.8;claim;]] .. S("Claim inventory") .. [[]

        tablecolumns[color;text;text;text;text;text;text;text;text;text;text]
        table[0.5,1.5;15,8;board;#999,]] ..
        S("ID") .. "," ..
        S("Title") .. "," ..
        S("Owner") .. "," ..
        S("Items") .. "," ..
        S("Initial bid") .. "," ..
        S("Increment") .. "," ..
        S("Fee") .. "," ..
        S("Max bid") .. "," ..
        S("Your bid") .. "," ..
        S("Finishing date")
    local formspec = { board_formspec }

    local auctions = auctions.get_auctions()

    for i, a in pairs(auctions) do
        local total_items = 0
        local player_bid = false
        for _, item in ipairs(a.items) do
            total_items = total_items + item.count
        end
        for _, bid in ipairs(a.bids) do
            if bid[2] == name then player_bid = bid[1] end
        end
        if os.time() > a.timeout and a.bids[#a.bids][2] == name and not a.claimed then
            formspec[#formspec+1] = "," .. "#ffd700"
        elseif os.time() > a.timeout then
            formspec[#formspec+1] = "," .. "#f33"
        elseif a.bids[#a.bids][2] == name then
            formspec[#formspec+1] = "," .. "#6cf"
        else
            formspec[#formspec+1] = "," .. "#fff"
        end
        formspec[#formspec+1] = ",#" .. a.uuid
        formspec[#formspec+1] = "," .. a.title
        formspec[#formspec+1] = "," .. a.owner
        formspec[#formspec+1] = "," .. tostring(total_items)
        formspec[#formspec+1] = "," .. tostring(a.bids[1][1])
        formspec[#formspec+1] = "," .. tostring(a.increment)
        formspec[#formspec+1] = "," .. tostring(a.fee)
        if #a.bids > 1 then
            formspec[#formspec+1] = "," .. tostring(a.bids[#a.bids][1])
        else
            formspec[#formspec+1] = "," .. "-"
        end
        formspec[#formspec+1] = "," .. tostring(player_bid or "-")
        formspec[#formspec+1] = "," .. os.date("%Y-%m-%d %X", a.timeout)
    end

    formspec[#formspec+1] = "]"

    minetest.show_formspec(name, "auctions:board", table.concat(formspec, ""))
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "auctions:board" then
        return
    end

    local name = player:get_player_name()

    if fields.new then
        auctions.show_new(name)
    elseif fields.board then
        local evt = minetest.explode_table_event(fields.board)
        if evt.type == "DCL" then
            local auction_id = auctions.get_auctions()[evt.row-1].uuid
            if auction_id and auctions.get_auction(auction_id) then
                auctions.selected_idxs.auction[name] = auction_id
                auctions.show_details(name, auction_id)
            end
        end
        return true
    elseif fields.claim then
        auctions.show_claim(name)
    end
end)
