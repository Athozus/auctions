function auctions.set_storage_entry(key, value)
    auctions.storage:set_string(key, minetest.serialize(value))
    return true
end

function auctions.get_storage_entry(key)
    return minetest.deserialize(auctions.storage:get_string(key) or "") or {}
end

function auctions.get_auctions()
    return auctions.get_storage_entry("auctions")
end

function auctions.new_auction(a)
    local entry = auctions.get_auctions()
    table.insert(entry, {
        uuid = a.id or auctions.new_uuid(),
        owner = a.owner,
        title = a.title,
        description = a.description,
        bids = { { a.bid, ":min" } },
        increment = a.increment,
        fee = a.fee,
        timeout = a.timeout or os.time() + (a.duration or 86400),
        items = a.items
    })
    return auctions.set_storage_entry("auctions", entry)
end

function auctions.get_auction(id)
    local entry = auctions.get_auctions()
    for _, a in ipairs(entry) do
        if a.uuid == id then
            return a
        end
    end
    return {}
end

function auctions.set_auction(id, auction)
    local entry = auctions.get_auctions()
    for i, a in ipairs(entry) do
        if a.uuid == id then
            entry[i] = auction
            return auctions.set_storage_entry("auctions", entry)
        end
    end
end

function auctions.new_bid(id, player, amount)
    local auction = auctions.get_auction(id)
    if os.time() <= auction.timeout then
        -- take money from the player
        if atm.balance[player] < auction.fee + amount then
            return false
        elseif amount < (#auction.bids == 1 and 0 or auction.increment) then
            return false
        end
        atm.balance[player] = atm.balance[player] - auction.fee - amount
        atm.balance[auction.owner] = atm.balance[auction.owner] + auction.fee
        -- return money to other bidders (except fee)
        local previous_bidders = {}
        for _, b in ipairs(auction.bids) do
            if string.sub(b[2], 1, 1) ~= ":" then
                previous_bidders[b[2]] = b[1] -- last bid
            end
        end
        for p, m in pairs(previous_bidders) do
            atm.balance[p] = atm.balance[p] + m
        end
        atm.saveaccounts()
        table.insert(auction.bids, { amount, player })
        return auctions.set_auction(id, auction)
    end
    return false
end

function auctions.get_claims()
    return auctions.get_storage_entry("claims") or {}
end

function auctions.get_claim(player)
    local entry = auctions.get_claims() or {}
    if entry[player] then return entry[player]
    else return {} end
end

function auctions.set_claim(player, items)
    local entry = auctions.get_claims()
    entry[player] = items
    return auctions.set_storage_entry("claims", entry)
end

function auctions.claim_auction(id, player)
    if #auctions.get_claim(player) == 0 then -- empty
        local auction = auctions.get_auction(id)
        if not auction.claimed then -- not already claimed
            local items = auction.items
            auction.claimed = true
            auctions.set_claim(player, items)
            auctions.set_auction(id, auction)
            atm.balance[auction.owner] = atm.balance[auction.owner] + auction.bids[#auction.bids][1]
            atm.saveaccounts()
            return true
        end
    end
end
