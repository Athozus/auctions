auctions = {
    storage = minetest.get_mod_storage(),

    selected_idxs = {
        auction = {},
    }
}

-- sub files
local MP = minetest.get_modpath(minetest.get_current_modname())
dofile(MP .. "/chatcommands.lua")
dofile(MP .. "/storage.lua")
dofile(MP .. "/ui/init.lua")
dofile(MP .. "/util/init.lua")
